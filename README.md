Description
===
Mini project to create a State Machine in LabVIEW.

<p align="center">
	<img src="assets/StateMachine.jpg" alt="State Machine" width="60%"/>
</p>


Video
===
![Demo Video](assets/Demo.mp4)
